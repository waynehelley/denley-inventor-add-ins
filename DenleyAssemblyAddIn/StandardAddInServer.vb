Imports Inventor
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports Microsoft.Win32

Namespace DenleyAssemblyAddIn
    <ProgIdAttribute("DenleyAssemblyAddIn.StandardAddInServer"),
    GuidAttribute("E6EA2F87-0494-4C0D-9E05-63DEB55637FB")>
    Public Class StandardAddInServer
        Implements Inventor.ApplicationAddInServer

#Region "Data Members"

        Private m_inventorApplication As Inventor.Application

        'buttons
        Private m_outputFittingListButton As OutputFittingList
        Private m_OutputTubeSizeList As OutputTubeSizeList
        Private m_GroundTubePipeButton As GroundTubePipeButton
        Private m_TubePipeVelocityCheckButton As TubePipeVelocityCheckButton

        'combo-boxes
        Private m_partNumberStartFromComboBoxDefinition As ComboBoxDefinition
        Private m_slotHeightComboBoxDefinition As ComboBoxDefinition

        'events
        Private m_userInterfaceEvents As UserInterfaceEvents

        ' ribbon panel
        Private m_assemblyToolsDenleyAssemblyRibbonPanel As RibbonPanel

#End Region

#Region "ApplicationAddInServer Members"

        Public Sub Activate(ByVal addInSiteObject As Inventor.ApplicationAddInSite, ByVal firstTime As Boolean) Implements Inventor.ApplicationAddInServer.Activate

            Try
                'the Activate method is called by Inventor when it loads the addin
                'the AddInSiteObject provides access to the Inventor Application object
                'the FirstTime flag indicates if the addin is loaded for the first time

                'initialize AddIn members
                m_inventorApplication = addInSiteObject.Application
                Button.InventorApplication = m_inventorApplication

                'initialize event handlers
                m_userInterfaceEvents = m_inventorApplication.UserInterfaceManager.UserInterfaceEvents

                AddHandler m_userInterfaceEvents.OnResetCommandBars, AddressOf Me.UserInterfaceEvents_OnResetCommandBars
                AddHandler m_userInterfaceEvents.OnResetEnvironments, AddressOf Me.UserInterfaceEvents_OnResetEnvironments
                AddHandler m_userInterfaceEvents.OnResetRibbonInterface, AddressOf Me.UserInterfaceEvents_OnResetRibbonInterface

                'load image icons for UI items
                Dim outputFittingListOptionImageStream As System.IO.Stream = Me.GetType().Assembly.GetManifestResourceStream("DenleyAssemblyAddIn.OutputFittingList.ico")
                Dim outputFittingListOptionIcon As Icon = New Icon(outputFittingListOptionImageStream)

                Dim outputTubeListImageStream As System.IO.Stream = Me.GetType().Assembly.GetManifestResourceStream("DenleyAssemblyAddIn.OutputTubeList.ico")
                Dim outputTubeListIcon As Icon = New Icon(outputTubeListImageStream)

                Dim GroundTubePipeImageStream As System.IO.Stream = Me.GetType().Assembly.GetManifestResourceStream("DenleyAssemblyAddIn.GroundTubePipe.ico")
                Dim GroundTubePipeIcon As Icon = New Icon(GroundTubePipeImageStream)

                Dim TubePipeVelocityCheckImageStream As System.IO.Stream = Me.GetType().Assembly.GetManifestResourceStream("DenleyAssemblyAddIn.TubePipeVelocityCheck.ico")
                Dim TubePipeVelocityCheckIcon As Icon = New Icon(TubePipeVelocityCheckImageStream)

                'retrieve the GUID for this class
                Dim addInCLSID As GuidAttribute
                addInCLSID = CType(System.Attribute.GetCustomAttribute(GetType(StandardAddInServer), GetType(GuidAttribute)), GuidAttribute)
                Dim addInCLSIDString As String
                addInCLSIDString = "{" & addInCLSID.Value & "}"

                'create the comboboxes
                m_partNumberStartFromComboBoxDefinition = m_inventorApplication.CommandManager.ControlDefinitions.AddComboBoxDefinition("Slot Width", "Autodesk:DenleyAssemblyAddIn:partNumberStartFromCboBox", CommandTypesEnum.kShapeEditCmdType, 100, addInCLSIDString, "Specifies slot width", "Slot width")
                m_slotHeightComboBoxDefinition = m_inventorApplication.CommandManager.ControlDefinitions.AddComboBoxDefinition("Slot Height", "Autodesk:DenleyAssemblyAddIn:SlotHeightCboBox", CommandTypesEnum.kShapeEditCmdType, 100, addInCLSIDString, "Specifies slot height", "Slot height")

                'add some initial items to the comboboxes
                m_partNumberStartFromComboBoxDefinition.AddItem("From next", 0)
                m_partNumberStartFromComboBoxDefinition.AddItem("From 1", 0)
                m_partNumberStartFromComboBoxDefinition.AddItem("None", 0)
                m_partNumberStartFromComboBoxDefinition.ListIndex = 1

                m_slotHeightComboBoxDefinition.AddItem("1 cm", 0)
                m_slotHeightComboBoxDefinition.AddItem("2 cm", 0)
                m_slotHeightComboBoxDefinition.AddItem("3 cm", 0)
                m_slotHeightComboBoxDefinition.AddItem("4 cm", 0)
                m_slotHeightComboBoxDefinition.AddItem("5 cm", 0)
                m_slotHeightComboBoxDefinition.ListIndex = 1

                'create buttons
                m_outputFittingListButton = New OutputFittingList("Output Fitting List", "Autodesk:DenleyAssemblyAddIn:outputFittingListOptionCmdBtn", CommandTypesEnum.kShapeEditCmdType,
                 addInCLSIDString, "Adds option for slot width/height", "Output Fitting List", outputFittingListOptionIcon, outputFittingListOptionIcon)

                m_OutputTubeSizeList = New OutputTubeSizeList("Output Tube List", "Autodesk:DenleyAssemblyAddIn:outputTubeListCmdBtn", CommandTypesEnum.kShapeEditCmdType,
                 addInCLSIDString, "Outputs tube data to JobSheet", "Output Tube List", outputTubeListIcon, outputTubeListIcon)

                m_GroundTubePipeButton = New GroundTubePipeButton("Ground Tube/Pipe", "Autodesk:DenleyAssemblyAddIn:GroundTubePipeCmdBtn", CommandTypesEnum.kShapeEditCmdType,
                 addInCLSIDString, "Enables/Disables state of slot command", "Ground Tube/Pipe", GroundTubePipeIcon, GroundTubePipeIcon)

                m_TubePipeVelocityCheckButton = New TubePipeVelocityCheckButton("Tube/Pipe Velocity Check", "Autodesk:DenleyAssemblyAddIn:TubePipeVelocityCheckCmdBtn", CommandTypesEnum.kShapeEditCmdType,
                 addInCLSIDString, "Enables/Disables state of slot command", "Tube/Pipe Velocity Check", TubePipeVelocityCheckIcon, TubePipeVelocityCheckIcon)

                'create the command category
                Dim slotCmdCategory As CommandCategory = m_inventorApplication.CommandManager.CommandCategories.Add("Denley", "Autodesk:DenleyAssemblyAddIn:SlotCmdCat", addInCLSIDString)

                'slotCmdCategory.Add(m_partNumberStartFromComboBoxDefinition)
                'slotCmdCategory.Add(m_slotHeightComboBoxDefinition)
                slotCmdCategory.Add(m_outputFittingListButton.ButtonDefinition)
                slotCmdCategory.Add(m_OutputTubeSizeList.ButtonDefinition)
                slotCmdCategory.Add(m_GroundTubePipeButton.ButtonDefinition)
                slotCmdCategory.Add(m_TubePipeVelocityCheckButton.ButtonDefinition)

                If firstTime = True Then

                    'access user interface manager
                    Dim userInterfaceManager As UserInterfaceManager
                    userInterfaceManager = m_inventorApplication.UserInterfaceManager

                    Dim interfaceStyle As InterfaceStyleEnum
                    interfaceStyle = userInterfaceManager.InterfaceStyle

                    m_assemblyToolsDenleyAssemblyRibbonPanel = Nothing

                    ' create the UI for classic interface
                    If interfaceStyle = InterfaceStyleEnum.kClassicInterface Then
                        'create toolbar
                        Dim slotCommandBar As CommandBar
                        slotCommandBar = userInterfaceManager.CommandBars.Add("Denley", "Autodesk:DenleyAssemblyAddIn:SlotToolbar", , addInCLSIDString)

                        'add comboboxes to toolbar
                        slotCommandBar.Controls.AddComboBox(m_partNumberStartFromComboBoxDefinition)
                        'slotCommandBar.Controls.AddComboBox(m_slotHeightComboBoxDefinition)

                        'add buttons to toolbar
                        slotCommandBar.Controls.AddButton(m_outputFittingListButton.ButtonDefinition)
                        slotCommandBar.Controls.AddButton(m_OutputTubeSizeList.ButtonDefinition)
                        slotCommandBar.Controls.AddButton(m_GroundTubePipeButton.ButtonDefinition)
                        slotCommandBar.Controls.AddButton(m_TubePipeVelocityCheckButton.ButtonDefinition)

                        'get the 2d sketch environment base object
                        Dim assemblyToolsEnvironment As Inventor.Environment
                        assemblyToolsEnvironment = userInterfaceManager.Environments.Item("AMxAssemblyEnvironment")

                        'make this command bar accessible in the panel menu for the 2d sketch environment
                        assemblyToolsEnvironment.PanelBar.CommandBarList.Add(slotCommandBar)

                        'create UI for ribbon interface
                    Else
                        ' get the ribbon associated with part document
                        Dim ribbons As Ribbons
                        ribbons = userInterfaceManager.Ribbons

                        Dim assemblyRibbon As Ribbon
                        assemblyRibbon = ribbons.Item("Assembly")

                        ' get the tabs associated with part ribbon
                        Dim ribbonTabs As RibbonTabs
                        ribbonTabs = assemblyRibbon.RibbonTabs

                        Dim assemblyToolsRibbonTab As RibbonTab
                        assemblyToolsRibbonTab = ribbonTabs.Item("id_TabTools")

                        ' create a new panel within the tab
                        Dim ribbonPanels As RibbonPanels
                        ribbonPanels = assemblyToolsRibbonTab.RibbonPanels

                        'Dim m_assemblyToolsDenleyAssemblyRibbonPanel As RibbonPanel
                        m_assemblyToolsDenleyAssemblyRibbonPanel = ribbonPanels.Add("Denley", "Autodesk:DenleyAssemblyAddIn:DenleyAssemblyRibbonPanel", "{DB59D9A7-EE4C-434A-BB5A-F93E8866E872}", "", False)

                        ' add controls to the slot panel
                        Dim assemblyToolsDenleyAssemblyRibbonPanelCtrls As CommandControls
                        assemblyToolsDenleyAssemblyRibbonPanelCtrls = m_assemblyToolsDenleyAssemblyRibbonPanel.CommandControls

                        ' add the combo boxes to the ribbon panel
                        'Dim partNumberStartFromCmdCboBoxCmdCtrl As CommandControl
                        'partNumberStartFromCmdCboBoxCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddComboBox(m_partNumberStartFromComboBoxDefinition, "", False)

                        'Dim slotHeightCmdCboBoxCmdCtrl As CommandControl
                        'slotHeightCmdCboBoxCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddComboBox(m_slotHeightComboBoxDefinition, "", False)

                        ' add the buttons to the ribbon panel
                        Dim outputFittingListCmdBtnCmdCtrl As CommandControl
                        outputFittingListCmdBtnCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddButton(m_outputFittingListButton.ButtonDefinition, False, True, "", False)

                        Dim outputTubeListCmdBtnCmdCtrl As CommandControl
                        outputTubeListCmdBtnCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddButton(m_OutputTubeSizeList.ButtonDefinition, False, True, "", False)

                        Dim GroundTubePipeCmdBtnCmdCtrl As CommandControl
                        GroundTubePipeCmdBtnCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddButton(m_GroundTubePipeButton.ButtonDefinition, False, True, "", False)

                        Dim TubePipeVelocityCheckCmdBtnCmdCtrl As CommandControl
                        TubePipeVelocityCheckCmdBtnCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddButton(m_TubePipeVelocityCheckButton.ButtonDefinition, False, True, "", False)



                    End If
                End If

            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try

        End Sub

        Public Sub Deactivate() Implements Inventor.ApplicationAddInServer.Deactivate

            'the Deactivate method is called by Inventor when the AddIn is unloaded
            'the AddIn will be unloaded either manually by the user or
            'when the Inventor session is terminated

            Try
                'release objects
                RemoveHandler m_userInterfaceEvents.OnResetCommandBars, AddressOf Me.UserInterfaceEvents_OnResetCommandBars
                RemoveHandler m_userInterfaceEvents.OnResetEnvironments, AddressOf Me.UserInterfaceEvents_OnResetEnvironments

                m_outputFittingListButton = Nothing
                m_OutputTubeSizeList = Nothing
                m_GroundTubePipeButton = Nothing
                m_TubePipeVelocityCheckButton = Nothing
                If Not m_assemblyToolsDenleyAssemblyRibbonPanel Is Nothing Then m_assemblyToolsDenleyAssemblyRibbonPanel.Delete()
                m_userInterfaceEvents = Nothing

                Marshal.ReleaseComObject(m_inventorApplication)
                m_inventorApplication = Nothing

                System.GC.WaitForPendingFinalizers()
                System.GC.Collect()

            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try

        End Sub

        Public ReadOnly Property Automation() As Object Implements Inventor.ApplicationAddInServer.Automation

            'if you want to return an interface to another client of this addin,
            'implement that interface in a class and return that class object 
            'through this property

            Get
                Return Nothing
            End Get

        End Property

        Public Sub ExecuteCommand(ByVal CommandID As Integer) Implements Inventor.ApplicationAddInServer.ExecuteCommand

            'this method was used to notify when an AddIn command was executed
            'the CommandID parameter identifies the command that was executed

            'Note:this method is now obsolete, you should use the new
            'ControlDefinition objects to implement commands, they have
            'their own event sinks to notify when the command is executed

        End Sub

        Public Sub UserInterfaceEvents_OnResetCommandBars(ByVal commandBars As ObjectsEnumerator, ByVal context As NameValueMap)

            Try
                Dim commandBar As CommandBar
                For Each commandBar In commandBars
                    If commandBar.InternalName = "Autodesk:DenleyAssemblyAddIn:SlotToolbar" Then

                        'add comboboxes to toolbar
                        'commandBar.Controls.AddComboBox(m_partNumberStartFromComboBoxDefinition)
                        'commandBar.Controls.AddComboBox(m_slotHeightComboBoxDefinition)

                        'add buttons to toolbar
                        commandBar.Controls.AddButton(m_outputFittingListButton.ButtonDefinition)
                        commandBar.Controls.AddButton(m_OutputTubeSizeList.ButtonDefinition)
                        commandBar.Controls.AddButton(m_GroundTubePipeButton.ButtonDefinition)
                        commandBar.Controls.AddButton(m_TubePipeVelocityCheckButton.ButtonDefinition)

                        Exit Sub
                    End If
                Next

            Catch
            End Try

        End Sub

        Public Sub UserInterfaceEvents_OnResetEnvironments(ByVal environments As ObjectsEnumerator, ByVal context As NameValueMap)

            Try
                Dim environment As Environment
                For Each environment In environments
                    'get the 2d sketch environment
                    If environment.InternalName = "AMxAssemblyEnvironment" Then

                        'make the command bar accessible in the panel menu for the 2d sketch environment
                        environment.PanelBar.CommandBarList.Add(m_inventorApplication.UserInterfaceManager.CommandBars.Item("Autodesk:DenleyAssemblyAddIn:SlotToolbar"))

                        Exit Sub
                    End If
                Next
            Catch
            End Try

        End Sub

        Public Sub UserInterfaceEvents_OnResetRibbonInterface(ByVal Context As Inventor.NameValueMap)
            ' get UserInterfaceManager
            Dim userInterfaceManager As UserInterfaceManager
            userInterfaceManager = m_inventorApplication.UserInterfaceManager

            ' get the ribbon associated with part documents
            Dim ribbons As Ribbons
            ribbons = userInterfaceManager.Ribbons

            Dim assemblyRibbon As Ribbon
            assemblyRibbon = ribbons.Item("Assembly")

            ' get the tabs associated with part ribbon
            Dim ribbonTabs As RibbonTabs
            ribbonTabs = assemblyRibbon.RibbonTabs

            Dim assemblyToolsRibbonTab As RibbonTab
            assemblyToolsRibbonTab = ribbonTabs.Item("id_TabTools")

            ' create a new panel within the tab
            Dim ribbonPanels As RibbonPanels
            ribbonPanels = assemblyToolsRibbonTab.RibbonPanels

            Dim assemblyToolsDenleyAssemblyRibbonPanel As RibbonPanel
            assemblyToolsDenleyAssemblyRibbonPanel = ribbonPanels.Add("Denley", "Autodesk:DenleyAssemblyAddIn:DenleyAssemblyRibbonPanel", "{DB59D9A7-EE4C-434A-BB5A-F93E8866E872}", "", False)

            ' add controls to the slot panel
            Dim assemblyToolsDenleyAssemblyRibbonPanelCtrls As CommandControls
            assemblyToolsDenleyAssemblyRibbonPanelCtrls = assemblyToolsDenleyAssemblyRibbonPanel.CommandControls

            ' add the combo boxes to the ribbon panel
            'Dim partNumberStartFromCmdCboBoxCmdCtrl As CommandControl
            'partNumberStartFromCmdCboBoxCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddComboBox(m_partNumberStartFromComboBoxDefinition, "", False)

            'Dim slotHeightCmdCboBoxCmdCtrl As CommandControl
            'slotHeightCmdCboBoxCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddComboBox(m_slotHeightComboBoxDefinition, "", False)

            ' add the buttons to the ribbon panel
            Dim outputTubeListCmdBtnCmdCtrl As CommandControl
            outputTubeListCmdBtnCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddButton(m_OutputTubeSizeList.ButtonDefinition, False, True, "", False)


            Dim outputFittingListCmdBtnCmdCtrl As CommandControl
            outputFittingListCmdBtnCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddButton(m_outputFittingListButton.ButtonDefinition, False, True, "", False)

            Dim GroundTubePipeCmdBtnCmdCtrl As CommandControl
            GroundTubePipeCmdBtnCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddButton(m_GroundTubePipeButton.ButtonDefinition, False, True, "", False)

            Dim TubePipeVelocityCheckCmdBtnCmdCtrl As CommandControl
            TubePipeVelocityCheckCmdBtnCmdCtrl = assemblyToolsDenleyAssemblyRibbonPanelCtrls.AddButton(m_TubePipeVelocityCheckButton.ButtonDefinition, False, True, "", False)
        End Sub
#End Region


    End Class

End Namespace

