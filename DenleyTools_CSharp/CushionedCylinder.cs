﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventor;
using System.Windows.Forms;

namespace DenleyTools_CSharp
{
    public class CushionedCylinder
    {

        private AssemblyDocument invDoc;
        private Parameters invParams;
        private PropertySet designTrackingProperties;

        private double boreDiameter;
        private double rodDiameter;
        private String rodEnd;
        private String mountingStyle;
        private double stroke;
        private double rodExtension;
        private double trunionPosition;
        
        private String partNo;

        public CushionedCylinder(AssemblyDocument invDoc)
        {

            this.invDoc = invDoc;
            this.invParams = invDoc.ComponentDefinition.Parameters;
            this.designTrackingProperties = invDoc.PropertySets["Design Tracking Properties"];

            this.boreDiameter = invParams["BORE_DIAMETER"].Value * 10; //cm to mm
            this.rodDiameter = invParams["ROD_DIAMETER"].Value * 10; //cm to mm

            this.rodEnd = invParams["ROD_END"].Value.Split()[0];

            this.mountingStyle = invParams["MOUNTING_STYLE"].Value.Split()[0];

            this.stroke = invParams["STROKE"].Value * 10; //cm to mm
            this.rodExtension = invParams["ROD_EXTENSION"].Value * 10; //cm to mm
            this.trunionPosition = invParams["TRUNNION_POSITION"].Value * 10; //cm to mm

            SetPartNumber();
            ValidateStokeAndTrunnionPosition();

            string barrelFileName = RetrieveBarrelFileName(this.mountingStyle);
            ReplaceComponent("barrel", barrelFileName + ".iam");
        }
            

        private void SetPartNumber()
        {
            partNo = "M-" + boreDiameter.ToString() + "-" + rodDiameter.ToString() + "-" + rodEnd + "-" + mountingStyle + "x" + stroke;

            PropertySet designTrackingProperties = invDoc.PropertySets["Design Tracking Properties"];

            designTrackingProperties["Part Number"].Value = partNo;
        }

        private void ValidateStokeAndTrunnionPosition()
        {

                double minStroke = 0;
                double maxTrunnionPosition = 0;
                double minTrunnionPosition = 0;
                bool trunnionAdjusted = false;
                bool strokeAdjusted = false;

                if (mountingStyle == "18")
                {
                    switch (boreDiameter)
                    {
                        case 25:
                            if (stroke < 50)
                            {
                                stroke = 50;
                                strokeAdjusted = true;
                            }
                            maxTrunnionPosition = 85 + stroke;
                            minTrunnionPosition = 135;
                            break;
                        case 32:
                            if (stroke < 50)
                            {
                                stroke = 50;
                                strokeAdjusted = true;
                            }
                            maxTrunnionPosition = 100 + stroke;
                            minTrunnionPosition = 150;
                            break;
                        case 40:
                            if (stroke < 65)
                            {
                                stroke = 65;
                                strokeAdjusted = true;
                            }
                            maxTrunnionPosition = 110 + stroke;
                            minTrunnionPosition = 175;
                            break;
                        case 50:
                            if (stroke < 75)
                            {
                                stroke = 75;
                                strokeAdjusted = true;
                            }
                            maxTrunnionPosition = 110 + stroke;
                            minTrunnionPosition = 185;
                            break;
                        case 63:
                            if (stroke < 75)
                            {
                                stroke = 75;
                                strokeAdjusted = true;
                            }
                            maxTrunnionPosition = 130 + stroke;
                            minTrunnionPosition = 205;
                            break;
                        case 80:
                            if (stroke < 95)
                            {
                                stroke = 95;
                                strokeAdjusted = true;
                            }
                            maxTrunnionPosition = 145 + stroke;
                            minTrunnionPosition = 240;
                            break;
                        case 100:
                            if (stroke < 135)
                            {
                                stroke = 135;
                                strokeAdjusted = true;
                            }
                            maxTrunnionPosition = 165 + stroke;
                            minTrunnionPosition = 300;
                            break;
                        case 125:
                            if (stroke < 130)
                            {
                                stroke = 130;
                                strokeAdjusted = true;
                            }
                            maxTrunnionPosition = 180 + stroke;
                            minTrunnionPosition = 310;
                            break;
                        case 160:
                            if (stroke < 175)
                            {
                                stroke = 175;
                                strokeAdjusted = true;
                            }
                            maxTrunnionPosition = 200 + stroke;
                            minTrunnionPosition = 370;
                            break;
                    }

                    if (trunionPosition > maxTrunnionPosition)
                    {
                        trunionPosition = maxTrunnionPosition;
                        trunnionAdjusted = true;
                    }
                    else if (trunionPosition < minTrunnionPosition)
                    {
                        trunionPosition = minTrunnionPosition;
                        trunnionAdjusted = true;
                    }

                }
                else
                {
                    if (stroke < 60)
                    {
                        stroke = 60;
                        strokeAdjusted = true;
                    }
                }

            }

        private string RetrieveBarrelFileName(string mountingStyle)
        {
            switch (mountingStyle)
            {                
                case "7a":                    
                case "9":
                case "18a":
                    return "barrel-plain";                
                default:
                    return "barrel-style " + mountingStyle;
            }

        }

        private void ReplaceComponent(string componentBrowserName, string replacementFileName)
        {

            ComponentOccurrence component = invDoc.ComponentDefinition.Occurrences.ItemByName[componentBrowserName];

            string[] arry = component.Definition.Document.FullFileName.Split('\\'); //split by backslash

            string folderName = "";

            for(int i=0; i<arry.Length-1;i++)
            {
                folderName = folderName + arry[i]+ "\\";
            }

            string fullFileName = folderName + replacementFileName;
            
            component.Replace(fullFileName, false);

        }

    }    
    


    }

