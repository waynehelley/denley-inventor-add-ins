Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("DenleyDrawingAddIn")>
<Assembly: AssemblyDescription("DenleyDrawingAddIn")>
<Assembly: AssemblyCompany("Wayne Helley")>
<Assembly: AssemblyProduct("Autodesk Inventor")>
<Assembly: AssemblyCopyright("")>
<Assembly: AssemblyTrademark("")> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("FCC7E39F-6E13-41F6-A019-D8DD3F7D16F2")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("0.0.3.*")>
