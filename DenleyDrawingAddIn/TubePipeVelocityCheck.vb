Imports Inventor
Imports System.Drawing
Imports System.Windows.Forms

Friend Class TubePipeVelocityCheckButton
    Inherits Button

#Region "Methods"

    Public Sub New(ByVal displayName As String,
                    ByVal internalName As String,
                    ByVal commandType As CommandTypesEnum,
                    ByVal clientId As String,
                    Optional ByVal description As String = "",
                    Optional ByVal tooltip As String = "",
                    Optional ByVal standardIcon As Icon = Nothing,
                    Optional ByVal largeIcon As Icon = Nothing,
                    Optional ByVal buttonDisplayType As ButtonDisplayEnum = ButtonDisplayEnum.kDisplayTextInLearningMode)

        MyBase.New(displayName, internalName, commandType, clientId, description, tooltip, standardIcon, largeIcon, buttonDisplayType)

    End Sub

    Protected Overrides Sub ButtonDefinition_OnExecute(ByVal context As Inventor.NameValueMap)

        DenleyTools.TubeHoseVelocityCheck()

    End Sub

#End Region

End Class
