Imports Inventor
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports Microsoft.Win32

Namespace DenleyDrawingAddIn
    <ProgIdAttribute("DenleyDrawingAddIn.StandardAddInServer"),
    GuidAttribute("E6EA2F87-0494-4C0D-9E05-63DEB55637FC")>
    Public Class StandardAddInServer
        Implements Inventor.ApplicationAddInServer

#Region "Data Members"

        Private m_inventorApplication As Inventor.Application

        'buttons
        Private m_DrawingPropertiesUpdate As DrawingPropertiesUpdate
        Private m_placeJobSheetButton As PlaceJobSheet
        'Private m_TubePipeVelocityCheckButton As TubePipeVelocityCheckButton

        'combo-boxes
        'Private m_partNumberStartFromComboBoxDefinition As ComboBoxDefinition
        'Private m_slotHeightComboBoxDefinition As ComboBoxDefinition

        'events
        Private m_userInterfaceEvents As UserInterfaceEvents

        ' ribbon panel
        Private m_drawingToolsDenleyDrawingRibbonPanel As RibbonPanel

#End Region

#Region "ApplicationAddInServer Members"

        Public Sub Activate(ByVal addInSiteObject As Inventor.ApplicationAddInSite, ByVal firstTime As Boolean) Implements Inventor.ApplicationAddInServer.Activate

            Try
                'the Activate method is called by Inventor when it loads the addin
                'the AddInSiteObject provides access to the Inventor Application object
                'the FirstTime flag indicates if the addin is loaded for the first time

                'initialize AddIn members
                m_inventorApplication = addInSiteObject.Application
                Button.InventorApplication = m_inventorApplication

                'initialize event handlers
                m_userInterfaceEvents = m_inventorApplication.UserInterfaceManager.UserInterfaceEvents

                AddHandler m_userInterfaceEvents.OnResetCommandBars, AddressOf Me.UserInterfaceEvents_OnResetCommandBars
                AddHandler m_userInterfaceEvents.OnResetEnvironments, AddressOf Me.UserInterfaceEvents_OnResetEnvironments
                AddHandler m_userInterfaceEvents.OnResetRibbonInterface, AddressOf Me.UserInterfaceEvents_OnResetRibbonInterface

                'load image icons for UI items

                Dim DrawingPropertiesUpdateImageStream As System.IO.Stream = Me.GetType().Assembly.GetManifestResourceStream("DenleyDrawingAddIn.DrawingPropertiesUpdate.ico")
                Dim DrawingPropertiesUpdateIcon As Icon = New Icon(DrawingPropertiesUpdateImageStream)

                Dim placeJobSheetOptionImageStream As System.IO.Stream = Me.GetType().Assembly.GetManifestResourceStream("DenleyDrawingAddIn.PlaceJobSheet.ico")
                Dim placeJobSheetOptionIcon As Icon = New Icon(placeJobSheetOptionImageStream)

                Dim TubePipeVelocityCheckImageStream As System.IO.Stream = Me.GetType().Assembly.GetManifestResourceStream("DenleyDrawingAddIn.TubePipeVelocityCheck.ico")
                Dim TubePipeVelocityCheckIcon As Icon = New Icon(TubePipeVelocityCheckImageStream)

                'retrieve the GUID for this class
                Dim addInCLSID As GuidAttribute
                addInCLSID = CType(System.Attribute.GetCustomAttribute(GetType(StandardAddInServer), GetType(GuidAttribute)), GuidAttribute)
                Dim addInCLSIDString As String
                addInCLSIDString = "{" & addInCLSID.Value & "}"

                'create the comboboxes
                'm_partNumberStartFromComboBoxDefinition = m_inventorApplication.CommandManager.ControlDefinitions.AddComboBoxDefinition("Slot Width", "Autodesk:DenleyDrawingAddIn:partNumberStartFromCboBox", CommandTypesEnum.kShapeEditCmdType, 100, addInCLSIDString, "Specifies slot width", "Slot width")
                'm_slotHeightComboBoxDefinition = m_inventorApplication.CommandManager.ControlDefinitions.AddComboBoxDefinition("Slot Height", "Autodesk:DenleyDrawingAddIn:SlotHeightCboBox", CommandTypesEnum.kShapeEditCmdType, 100, addInCLSIDString, "Specifies slot height", "Slot height")

                'add some initial items to the comboboxes
                'm_partNumberStartFromComboBoxDefinition.AddItem("From next", 0)
                'm_partNumberStartFromComboBoxDefinition.AddItem("From 1", 0)
                'm_partNumberStartFromComboBoxDefinition.AddItem("None", 0)
                'm_partNumberStartFromComboBoxDefinition.ListIndex = 1

                'm_slotHeightComboBoxDefinition.AddItem("1 cm", 0)
                'm_slotHeightComboBoxDefinition.AddItem("2 cm", 0)
                'm_slotHeightComboBoxDefinition.AddItem("3 cm", 0)
                'm_slotHeightComboBoxDefinition.AddItem("4 cm", 0)
                'm_slotHeightComboBoxDefinition.AddItem("5 cm", 0)
                'm_slotHeightComboBoxDefinition.ListIndex = 1

                'create buttons
                m_DrawingPropertiesUpdate = New DrawingPropertiesUpdate("Drawing Properties Update", "Autodesk:DenleyDrawingAddIn:DrawingPropertiesUpdateCmdBtn", CommandTypesEnum.kShapeEditCmdType,
                addInCLSIDString, "Outputs tube data to JobSheet", "Drawing Properties Update", DrawingPropertiesUpdateIcon, DrawingPropertiesUpdateIcon)

                m_placeJobSheetButton = New PlaceJobSheet("Place Job Sheet", "Autodesk:DenleyDrawingAddIn:placeJobSheetOptionCmdBtn", CommandTypesEnum.kShapeEditCmdType,
                 addInCLSIDString, "Adds option for slot width/height", "Place Job Sheet", placeJobSheetOptionIcon, placeJobSheetOptionIcon)

                'm_TubePipeVelocityCheckButton = New TubePipeVelocityCheckButton("Tube/Pipe Velocity Check", "Autodesk:DenleyDrawingAddIn:TubePipeVelocityCheckCmdBtn", CommandTypesEnum.kShapeEditCmdType,
                'addInCLSIDString, "Enables/Disables state of slot command", "Tube/Pipe Velocity Check", TubePipeVelocityCheckIcon, TubePipeVelocityCheckIcon)

                'create the command category
                Dim slotCmdCategory As CommandCategory = m_inventorApplication.CommandManager.CommandCategories.Add("Denley", "Autodesk:DenleyDrawingAddIn:SlotCmdCat", addInCLSIDString)

                'slotCmdCategory.Add(m_partNumberStartFromComboBoxDefinition)
                'slotCmdCategory.Add(m_slotHeightComboBoxDefinition)
                slotCmdCategory.Add(m_DrawingPropertiesUpdate.ButtonDefinition)
                slotCmdCategory.Add(m_placeJobSheetButton.ButtonDefinition)
                'slotCmdCategory.Add(m_TubePipeVelocityCheckButton.ButtonDefinition)

                If firstTime = True Then

                    'access user interface manager
                    Dim userInterfaceManager As UserInterfaceManager
                    userInterfaceManager = m_inventorApplication.UserInterfaceManager

                    Dim interfaceStyle As InterfaceStyleEnum
                    interfaceStyle = userInterfaceManager.InterfaceStyle

                    m_drawingToolsDenleyDrawingRibbonPanel = Nothing

                    ' create the UI for classic interface
                    If interfaceStyle = InterfaceStyleEnum.kClassicInterface Then
                        'create toolbar
                        Dim slotCommandBar As CommandBar
                        slotCommandBar = userInterfaceManager.CommandBars.Add("Denley", "Autodesk:DenleyDrawingAddIn:SlotToolbar", , addInCLSIDString)

                        'add comboboxes to toolbar
                        'slotCommandBar.Controls.AddComboBox(m_partNumberStartFromComboBoxDefinition)
                        'slotCommandBar.Controls.AddComboBox(m_slotHeightComboBoxDefinition)

                        'add buttons to toolbar
                        slotCommandBar.Controls.AddButton(m_DrawingPropertiesUpdate.ButtonDefinition)
                        slotCommandBar.Controls.AddButton(m_placeJobSheetButton.ButtonDefinition)
                        'slotCommandBar.Controls.AddButton(m_TubePipeVelocityCheckButton.ButtonDefinition)

                        'get the 2d sketch environment base object
                        Dim drawingToolsEnvironment As Inventor.Environment
                        drawingToolsEnvironment = userInterfaceManager.Environments.Item("AMxDrawingEnvironment")

                        'make this command bar accessible in the panel menu for the 2d sketch environment
                        drawingToolsEnvironment.PanelBar.CommandBarList.Add(slotCommandBar)

                        'create UI for ribbon interface
                    Else
                        ' get the ribbon associated with part document
                        Dim ribbons As Ribbons
                        ribbons = userInterfaceManager.Ribbons

                        Dim drawingRibbon As Ribbon
                        drawingRibbon = ribbons.Item("Drawing")

                        ' get the tabs associated with part ribbon
                        Dim ribbonTabs As RibbonTabs
                        ribbonTabs = drawingRibbon.RibbonTabs

                        Dim drawingToolsRibbonTab As RibbonTab
                        drawingToolsRibbonTab = ribbonTabs.Item("id_TabTools")

                        ' create a new panel within the tab
                        Dim ribbonPanels As RibbonPanels
                        ribbonPanels = drawingToolsRibbonTab.RibbonPanels

                        'Dim m_drawingToolsDenleyDrawingRibbonPanel As RibbonPanel
                        m_drawingToolsDenleyDrawingRibbonPanel = ribbonPanels.Add("Denley", "Autodesk:DenleyDrawingAddIn:DenleyDrawingRibbonPanel", "{DB59D9A7-EE4C-434A-BB5A-F93E8866E872}", "", False)

                        ' add controls to the slot panel
                        Dim drawingToolsDenleyDrawingRibbonPanelCtrls As CommandControls
                        drawingToolsDenleyDrawingRibbonPanelCtrls = m_drawingToolsDenleyDrawingRibbonPanel.CommandControls

                        ' add the combo boxes to the ribbon panel
                        'Dim partNumberStartFromCmdCboBoxCmdCtrl As CommandControl
                        'partNumberStartFromCmdCboBoxCmdCtrl = drawingToolsDenleyDrawingRibbonPanelCtrls.AddComboBox(m_partNumberStartFromComboBoxDefinition, "", False)

                        'Dim slotHeightCmdCboBoxCmdCtrl As CommandControl
                        'slotHeightCmdCboBoxCmdCtrl = drawingToolsDenleyDrawingRibbonPanelCtrls.AddComboBox(m_slotHeightComboBoxDefinition, "", False)

                        ' add the buttons to the ribbon panel
                        Dim DrawingPropertiesUpdateCmdBtnCmdCtrl As CommandControl
                        DrawingPropertiesUpdateCmdBtnCmdCtrl = drawingToolsDenleyDrawingRibbonPanelCtrls.AddButton(m_DrawingPropertiesUpdate.ButtonDefinition, False, True, "", False)

                        Dim placeJobSheetCmdBtnCmdCtrl As CommandControl
                        placeJobSheetCmdBtnCmdCtrl = drawingToolsDenleyDrawingRibbonPanelCtrls.AddButton(m_placeJobSheetButton.ButtonDefinition, False, True, "", False)

                        ' Dim TubePipeVelocityCheckCmdBtnCmdCtrl As CommandControl
                        'TubePipeVelocityCheckCmdBtnCmdCtrl = drawingToolsDenleyDrawingRibbonPanelCtrls.AddButton(m_TubePipeVelocityCheckButton.ButtonDefinition, False, True, "", False)



                    End If
                End If

            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try

        End Sub

        Public Sub Deactivate() Implements Inventor.ApplicationAddInServer.Deactivate

            'the Deactivate method is called by Inventor when the AddIn is unloaded
            'the AddIn will be unloaded either manually by the user or
            'when the Inventor session is terminated

            Try
                'release objects
                RemoveHandler m_userInterfaceEvents.OnResetCommandBars, AddressOf Me.UserInterfaceEvents_OnResetCommandBars
                RemoveHandler m_userInterfaceEvents.OnResetEnvironments, AddressOf Me.UserInterfaceEvents_OnResetEnvironments

                m_DrawingPropertiesUpdate = Nothing
                m_placeJobSheetButton = Nothing
                'm_TubePipeVelocityCheckButton = Nothing
                If Not m_drawingToolsDenleyDrawingRibbonPanel Is Nothing Then m_drawingToolsDenleyDrawingRibbonPanel.Delete()
                m_userInterfaceEvents = Nothing

                Marshal.ReleaseComObject(m_inventorApplication)
                m_inventorApplication = Nothing

                System.GC.WaitForPendingFinalizers()
                System.GC.Collect()

            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try

        End Sub

        Public ReadOnly Property Automation() As Object Implements Inventor.ApplicationAddInServer.Automation

            'if you want to return an interface to another client of this addin,
            'implement that interface in a class and return that class object 
            'through this property

            Get
                Return Nothing
            End Get

        End Property

        Public Sub ExecuteCommand(ByVal CommandID As Integer) Implements Inventor.ApplicationAddInServer.ExecuteCommand

            'this method was used to notify when an AddIn command was executed
            'the CommandID parameter identifies the command that was executed

            'Note:this method is now obsolete, you should use the new
            'ControlDefinition objects to implement commands, they have
            'their own event sinks to notify when the command is executed

        End Sub

        Public Sub UserInterfaceEvents_OnResetCommandBars(ByVal commandBars As ObjectsEnumerator, ByVal context As NameValueMap)

            Try
                Dim commandBar As CommandBar
                For Each commandBar In commandBars
                    If commandBar.InternalName = "Autodesk:DenleyDrawingAddIn:SlotToolbar" Then

                        'add comboboxes to toolbar
                        'commandBar.Controls.AddComboBox(m_partNumberStartFromComboBoxDefinition)
                        'commandBar.Controls.AddComboBox(m_slotHeightComboBoxDefinition)

                        'add buttons to toolbar
                        commandBar.Controls.AddButton(m_DrawingPropertiesUpdate.ButtonDefinition)
                        commandBar.Controls.AddButton(m_placeJobSheetButton.ButtonDefinition)
                        'commandBar.Controls.AddButton(m_TubePipeVelocityCheckButton.ButtonDefinition)

                        Exit Sub
                    End If
                Next

            Catch
            End Try

        End Sub

        Public Sub UserInterfaceEvents_OnResetEnvironments(ByVal environments As ObjectsEnumerator, ByVal context As NameValueMap)

            Try
                Dim environment As Environment
                For Each environment In environments
                    'get the 2d sketch environment
                    If environment.InternalName = "AMxDrawingEnvironment" Then

                        'make the command bar accessible in the panel menu for the 2d sketch environment
                        environment.PanelBar.CommandBarList.Add(m_inventorApplication.UserInterfaceManager.CommandBars.Item("Autodesk:DenleyDrawingAddIn:SlotToolbar"))

                        Exit Sub
                    End If
                Next
            Catch
            End Try

        End Sub

        Public Sub UserInterfaceEvents_OnResetRibbonInterface(ByVal Context As Inventor.NameValueMap)
            ' get UserInterfaceManager
            Dim userInterfaceManager As UserInterfaceManager
            userInterfaceManager = m_inventorApplication.UserInterfaceManager

            ' get the ribbon associated with part documents
            Dim ribbons As Ribbons
            ribbons = userInterfaceManager.Ribbons

            Dim drawingRibbon As Ribbon
            drawingRibbon = ribbons.Item("Drawing")

            ' get the tabs associated with part ribbon
            Dim ribbonTabs As RibbonTabs
            ribbonTabs = drawingRibbon.RibbonTabs

            Dim drawingToolsRibbonTab As RibbonTab
            drawingToolsRibbonTab = ribbonTabs.Item("id_TabTools")

            ' create a new panel within the tab
            Dim ribbonPanels As RibbonPanels
            ribbonPanels = drawingToolsRibbonTab.RibbonPanels

            Dim drawingToolsDenleyDrawingRibbonPanel As RibbonPanel
            drawingToolsDenleyDrawingRibbonPanel = ribbonPanels.Add("Denley", "Autodesk:DenleyDrawingAddIn:DenleyDrawingRibbonPanel", "{DB59D9A7-EE4C-434A-BB5A-F93E8866E872}", "", False)

            ' add controls to the slot panel
            Dim drawingToolsDenleyDrawingRibbonPanelCtrls As CommandControls
            drawingToolsDenleyDrawingRibbonPanelCtrls = drawingToolsDenleyDrawingRibbonPanel.CommandControls

            ' add the combo boxes to the ribbon panel
            'Dim partNumberStartFromCmdCboBoxCmdCtrl As CommandControl
            'partNumberStartFromCmdCboBoxCmdCtrl = drawingToolsDenleyDrawingRibbonPanelCtrls.AddComboBox(m_partNumberStartFromComboBoxDefinition, "", False)

            'Dim slotHeightCmdCboBoxCmdCtrl As CommandControl
            'slotHeightCmdCboBoxCmdCtrl = drawingToolsDenleyDrawingRibbonPanelCtrls.AddComboBox(m_slotHeightComboBoxDefinition, "", False)

            ' add the buttons to the ribbon panel
            Dim DrawingPropertiesUpdateCmdBtnCmdCtrl As CommandControl
            DrawingPropertiesUpdateCmdBtnCmdCtrl = drawingToolsDenleyDrawingRibbonPanelCtrls.AddButton(m_DrawingPropertiesUpdate.ButtonDefinition, False, True, "", False)


            Dim placeJobSheetCmdBtnCmdCtrl As CommandControl
            placeJobSheetCmdBtnCmdCtrl = drawingToolsDenleyDrawingRibbonPanelCtrls.AddButton(m_placeJobSheetButton.ButtonDefinition, False, True, "", False)

            'Dim TubePipeVelocityCheckCmdBtnCmdCtrl As CommandControl
            'TubePipeVelocityCheckCmdBtnCmdCtrl = drawingToolsDenleyDrawingRibbonPanelCtrls.AddButton(m_TubePipeVelocityCheckButton.ButtonDefinition, False, True, "", False)
        End Sub
#End Region


    End Class

End Namespace

