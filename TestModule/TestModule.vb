﻿Imports System
Imports System.Type
Imports System.Activator
Imports System.Runtime.InteropServices
Imports Inventor
Imports DenleyTools
Imports DenleyTools_CSharp
Imports System.Windows.Forms

Module TestModule

    Dim _invApp As Inventor.Application
    Dim _started As Boolean

    Sub Main()

        ' Add any initialization after the InitializeComponent() call.
        Try
            _invApp = Marshal.GetActiveObject("Inventor.Application")

        Catch ex As Exception
            Try
                Dim invAppType As Type =
                  GetTypeFromProgID("Inventor.Application")

                _invApp = CreateInstance(invAppType)
                _invApp.Visible = True

                'Note: if you shut down the Inventor session that was started
                'this(way) there is still an Inventor.exe running. We will use
                'this Boolean to test whether or not the Inventor App  will
                'need to be shut down.
                _started = True

            Catch ex2 As Exception
                MsgBox(ex2.ToString())
                MsgBox("Unable to get or start Inventor")
            End Try
        End Try

        Dim invDoc As Document = _invApp.ActiveDocument

        '_invApp.UserInterfaceManager.UserInteractionDisabled = True

        'DenleyTools.PlaceInventorPartsList(invDoc)
        'BillOfMaterialsCrossReference(invDoc)
        'DetectDocumentTypeInDrawing(invDoc)
        'DenleyTools.OutputTubeSizeList(invDoc)
        'DenleyTools.TubeHoseVelocityCheck()
        'DenleyTools.OutputFittingList(invDoc)
        'DenleyTools.OpenJobSheet(invDoc)
        'DenleyTools.DrawingPropertiesUpdate(invDoc)
        'DenleyTools.GroundTubePipe(invDoc)
        'DenleyTools.TableBreaker(invDoc)

        Dim cylinder As New DenleyTools_CSharp.CushionedCylinder(invDoc)

        'Dim frm As New TubePipeVelocityForm
        'System.Windows.Forms.Application.Run(frm)

        '_invApp.UserInterfaceManager.UserInteractionDisabled = False

        'Release references
        _invApp = Nothing

    End Sub

End Module