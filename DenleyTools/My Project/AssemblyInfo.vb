﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("DenleyTools")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Hewlett-Packard Company")>
<Assembly: AssemblyProduct("DenleyTools")>
<Assembly: AssemblyCopyright("Copyright © Hewlett-Packard Company 2017")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("bf9c6a03-f566-4197-97b2-aae9dd8ec676")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.1.2")>
<Assembly: AssemblyFileVersion("1.0.1.2")>
