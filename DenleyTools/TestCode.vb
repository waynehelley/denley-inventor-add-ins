﻿Imports Inventor

Public Module TestCode

    Sub DetectDocumentTypeInDrawing(oDrawingDoc As DrawingDocument)

        Dim oSheet As Sheet
        oSheet = oDrawingDoc.ActiveSheet

        Dim oDrawingView As DrawingView
        oDrawingView = oSheet.DrawingViews(1)

        Dim oRefDocType As DocumentTypeEnum

        oRefDocType = oDrawingView.ReferencedDocumentDescriptor.ReferencedDocumentType

        Select Case oRefDocType
            Case DocumentTypeEnum.kPartDocumentObject
                MsgBox("This is a part")
            Case DocumentTypeEnum.kAssemblyDocumentObject
                MsgBox("This is an assembly")
        End Select

    End Sub

    Sub SuppressionMatcher(assDoc As AssemblyDocument)

        Dim subAssDoc1 As AssemblyDocument = assDoc.ComponentDefinition.Occurrences(1).Definition.Document
        Dim subAssDoc2 As AssemblyDocument = assDoc.ComponentDefinition.Occurrences(2).Definition.Document

        If subAssDoc2.LevelOfDetailName = "Master" Then
            MsgBox("This logic only works if subassembly 2 is not set to master Level of Detail")
            Exit Sub
        End If

        Dim noOfComponentsInEachAssembly As Integer = subAssDoc1.ComponentDefinition.Occurrences.Count
        For i = 1 To noOfComponentsInEachAssembly
            If subAssDoc1.ComponentDefinition.Occurrences(i).Suppressed Then
                subAssDoc2.ComponentDefinition.Occurrences(i).Suppress()
            Else
                subAssDoc2.ComponentDefinition.Occurrences(i).Unsuppress()
            End If
        Next

    End Sub

End Module