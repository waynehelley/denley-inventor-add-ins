﻿Imports Inventor

Public Module Drawing

    Public Sub DrawingPropertiesUpdate(invDoc As DrawingDocument)

        'This rule updates the JobNo, DrgNo and SheetNo in a drawing based on the FileName and Path

        Dim filenameArr0 As String() = invDoc.FullFileName.Split("\")
        Dim fileName As String = filenameArr0(filenameArr0.Length - 1)
        fileName = Left(fileName, fileName.Length - 4)

        'File must be saved before this script can run
        If fileName = "" Then
            MsgBox("Please Save the file first")
            Exit Sub
        End If

        Dim filenameArr As String() = fileName.Split("-")

        Dim DrgNo As String = filenameArr(0)

        Dim SheetNo As String = filenameArr(filenameArr.Length - 1)

        Dim View1Scale As String

        Try 'Will fail if no views
            View1Scale = invDoc.ActiveSheet.DrawingViews(1).ScaleString
        Catch
            View1Scale = ""
        End Try

        Dim titleBlock As TitleBlock = invDoc.ActiveSheet.TitleBlock

        For Each tb In titleBlock.Definition.Sketch.TextBoxes
            If tb.Text = "<DRAWING No.>" Then
                titleBlock.SetPromptResultText(tb, DrgNo)
            ElseIf tb.Text = "<Sheet number>" Then
                titleBlock.SetPromptResultText(tb, SheetNo)
            ElseIf tb.Text = "<SCALE>" Then
                If titleBlock.GetResultText(tb) <> "DNS" Then 'DNS = Do not scale
                    titleBlock.SetPromptResultText(tb, View1Scale)
                End If
            End If
        Next

        Dim pathAndFileName As String = Left(invDoc.FullFileName, invDoc.FullFileName.Length - 4)

        Dim filepathArr As String() = pathAndFileName.Split("\")

        Dim folderName As String = filepathArr(filepathArr.Length - 2)

        Dim invCustProps As PropertySet = invDoc.PropertySets.Item("User Defined Properties")

        Dim JobNo As String = Left(folderName, 5)

        If Char.IsLetter(folderName.Chars(5)) Then
            JobNo = JobNo & folderName.Chars(5)
        End If

        invCustProps.Item("JOB#").Value = JobNo

        Dim modelFileName As String = ""

        Try
            Dim modelFileNameArr As String() = invDoc.ActiveSheet.DrawingViews.Item(1).ReferencedDocumentDescriptor.FullDocumentName.Split("\")
            modelFileName = modelFileNameArr(modelFileNameArr.Length - 1)
        Catch

        End Try

        Dim TopLevel As Boolean

        If modelFileName.ToUpper.Contains("TOP LEVEL") Then
            TopLevel = True
        End If

        Dim summaryPropSet As PropertySet
        summaryPropSet = invDoc.PropertySets.Item("Summary Information")

        Dim designPropSet As PropertySet
        designPropSet = invDoc.PropertySets.Item("Design Tracking Properties")

        Dim companyProp As [Property]
        companyProp = invDoc.PropertySets.Item("Inventor Document Summary Information").Item("Company")

        If TopLevel Then
            Try 'Will fail if there are no brackets in folder name
                'Grab portion of Folder Name which resides in brackets
                Dim i As Integer = folderName.IndexOf("(")
                Dim JobName As String = folderName.Substring(i + 1, folderName.IndexOf(")", i + 1) - i - 1).ToUpper

                Dim JobNameArr = JobName.Split("-")

                Select Case JobNameArr(0).Trim()
                    Case "FARREL"
                        companyProp.Value = JobNameArr(0).Trim() & " LTD."
                    Case Else
                        companyProp.Value = JobNameArr(0).Trim()
                End Select

                designPropSet.Item("Project").Value = Right(JobName, JobName.Length - JobNameArr(0).Length - 1).Trim()

            Catch
            End Try
        Else
            summaryPropSet.Item("Comments").Value = ""
            companyProp.Value = ""
            designPropSet.Item("Project").Value = ""
        End If

        'Title Logic
        If summaryPropSet.Item("Title").Value = "" Then

            If TopLevel Then
                summaryPropSet.Item("Title").Value = "GENERAL ARRANGEMENT"
            ElseIf modelFileName.ToUpper.Contains("FRAME") Then
                summaryPropSet.Item("Title").Value = "FRAME DETAIL"
            ElseIf modelFileName.ToUpper.Contains("LID") Then
                summaryPropSet.Item("Title").Value = "TANK LID DETAIL"
            ElseIf modelFileName.ToUpper.Contains("TANK") Then
                summaryPropSet.Item("Title").Value = "TANK DETAIL"
            ElseIf modelFileName.ToUpper.Contains("MANIFOLD") Then
                summaryPropSet.Item("Title").Value = "MANIFOLD DETAIL"
            Else
                If modelFileName <> "" Then
                    summaryPropSet.Item("Title").Value = Mid(modelFileName, 8, modelFileName.Length - 11).ToUpper
                End If
            End If

            End If

        invDoc.Update()


    End Sub

End Module
