﻿Imports System.ComponentModel
Imports System.Windows.Forms

Public Class TubePipeVelocityForm
    Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub

    Public Sub TubePipeVelocityCheck_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ComboBox1.Items.Clear()
        ComboBox1.Items.Add("Safety")
        ComboBox1.Items.Add("Security")
        ComboBox1.Items.Add("Governance")
        ComboBox1.Items.Add("Good Music")
        ComboBox1.Items.Add("Good Movies")
        ComboBox1.Items.Add("Good Books")
        ComboBox1.Items.Add("Education")
        ComboBox1.Items.Add("Roads")

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedIndexChanged

        ComboBox1.Items.Clear()
        ComboBox1.Items.Add("Velocity")
        ComboBox1.Items.Add("Bore")
        ComboBox1.Items.Add("Flow Rate")

    End Sub
End Class