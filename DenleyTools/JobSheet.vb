﻿Imports Inventor
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Type
Imports System.Activator

Public Module JobSheet

    Dim pipeComponents As New List(Of Component)

    Dim fittingComponents As New List(Of ComponentSubList)

    Sub BillOfMaterialsCrossReference(invDoc As AssemblyDocument)

        'MsgBox(invDoc.FullFileName)

        Dim JobSheetComponents As List(Of Component) = RetrievePartsList("")

        'Console.Write("Press Enter to continue...")
        'Console.ReadLine()

        Dim oBom As BOM
        oBom = invDoc.ComponentDefinition.BOM
        oBom.PartsOnlyViewEnabled = True
        Dim oBomView As BOMView
        oBomView = oBom.BOMViews.Item("Parts Only")
        Dim oBOMRows As BOMRowsEnumerator
        oBOMRows = oBomView.BOMRows

        Dim InventorComponents As New List(Of Component)

        Dim j As Integer
        For j = 1 To oBOMRows.Count
            Dim rowDoc As Document
            rowDoc = oBOMRows.Item(j).ComponentDefinitions.Item(1).Document.ComponentDefinition.Document
            Dim comp As New Component
            comp.ItemNumber = oBOMRows.Item(j).ItemNumber
            comp.PartNumber = rowDoc.PropertySets.Item("Design Tracking Properties").Item("Part Number").Value
            If comp.PartNumber <> "" Then InventorComponents.Add(comp)
        Next j

        'For Each comp In InventorComponents
        '    Console.WriteLine(comp.ItemNumber & ": " & comp.PartNumber)
        'Next

        'Console.Write("Press Enter to continue...")
        'Console.ReadLine()

        For Each jobSheetComp In JobSheetComponents
            'Console.WriteLine(jobSheetComp.ItemNumber & ": " & jobSheetComp.PartNumber)
            For Each invComp In InventorComponents
                If jobSheetComp.PartNumber.Contains(invComp.PartNumber) Or invComp.PartNumber.Contains(jobSheetComp.PartNumber) Then
                    Console.WriteLine("Match found... " & jobSheetComp.ItemNumber & ": " & jobSheetComp.PartNumber & " matches " & invComp.ItemNumber & ": " & invComp.PartNumber)
                End If
            Next
        Next

        Console.Write("Press Enter to continue...")
        Console.ReadLine()

    End Sub

    Structure Component
        Public ItemNumber As String
        Public Description As String
        Public PartNumber As String
        Public Qty As String 'This might need to become a string for lengths (e.g. 0.5m)
    End Structure

    Class ComponentSubList

        Public listName As String
        Public components As List(Of Component)

        Sub New(aListName As String)
            listName = aListName
            components = New List(Of Component)
        End Sub

    End Class


    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try

    End Sub

    Private Sub BOMtest(oIamDoc As AssemblyDocument)

        Dim oIamCompDef As AssemblyComponentDefinition
        oIamCompDef = oIamDoc.ComponentDefinition
        Dim oBom As BOM
        oBom = oIamCompDef.BOM
        oBom.PartsOnlyViewEnabled = True
        Dim oBomView As BOMView
        oBomView = oBom.BOMViews.Item("Structured")
        Dim oBOMRows As BOMRowsEnumerator
        oBOMRows = oBomView.BOMRows
        Dim i As Integer
        For i = 1 To oBOMRows.Count
            Console.Write(oBOMRows.Item(i).ComponentDefinitions.Count) ' if more then one, it's merged
            Dim j As Integer
            For j = 1 To oBOMRows.Item(i).ComponentDefinitions.Count
                Console.Write(oBOMRows.Item(i).ComponentDefinitions.Item(j).Document.FullFileName)
            Next j
        Next i

    End Sub

    Private Function RetrievePartsList(JobSheet As String)

        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet

        Dim xlAppType As Type =
                  GetTypeFromProgID("Excel.Application")

        xlApp = CreateInstance(xlAppType)

        'Change security settings of Excel session to disable Jobsheet add-on
        xlApp.AutomationSecurity = Microsoft.Office.Core.MsoAutomationSecurity.msoAutomationSecurityForceDisable

        Dim JobSheetAndPath As String

        If JobSheet = "test1" Then
            JobSheetAndPath = "C:\Users\wayne\Documents\test1.xlsx"
        ElseIf JobSheet = "" Then
            Throw New Exception(message:="JobSheet Number Required")
        Else
            JobSheetAndPath = "\\denleyserver\jobsheets\" & JobSheet & ".xls"
        End If

        xlWorkBook = xlApp.Workbooks.Open(JobSheetAndPath, , vbReadOnly)
        xlWorkSheet = xlWorkBook.Worksheets("JobSheet")

        Dim FinalRow As Integer
        FinalRow = xlWorkSheet.Cells(xlWorkSheet.Rows.Count, 1).End(Excel.XlDirection.xlUp).Row

        Dim JobSheetComponents As New List(Of Component)

        With xlWorkSheet
            Dim i As Integer
            For i = 8 To FinalRow
                Dim comp As Component
                comp.ItemNumber = xlWorkSheet.Cells(i, 1).value 'Column 1 = Item Number

                Dim value As String = xlWorkSheet.Cells(i, 2).value 'Column 2 = Qty
                If IsNumeric(value) Then comp.Qty = value 'Column 2 = Qty

                comp.Description = xlWorkSheet.Cells(i, 3).value 'Column 3 = Description

                comp.PartNumber = xlWorkSheet.Cells(i, 4).value 'Column 4 = Part Number

                'Add to JobSheet components if at least 2 fields are filled, otherwise its probably just a header
                Dim fieldsFilled As Byte = 0

                'Ensure Nothing values don't exist otherwise it will cause the Parts List creation to fail
                If comp.ItemNumber Is Nothing Then comp.ItemNumber = ""
                If comp.PartNumber Is Nothing Then comp.PartNumber = ""
                If comp.Description Is Nothing Then comp.Description = ""

                If comp.ItemNumber <> "" Then fieldsFilled = fieldsFilled + 1
                If comp.PartNumber <> "" Then fieldsFilled = fieldsFilled + 1
                If comp.Description <> "" Then fieldsFilled = fieldsFilled + 1

                If fieldsFilled > 1 Then
                    JobSheetComponents.Add(comp)
                End If

            Next i
        End With

        'For Each comp In JobSheetComponents
        'Console.WriteLine(comp.ItemNumber & ": " & comp.PartNumber)
        'Next

        xlWorkBook.Close(False) ' False = Do not save
        xlApp.Quit()

        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)

        Return JobSheetComponents

    End Function

    Public Sub PlaceInventorPartsList(oDrawDoc As DrawingDocument)

        Dim jobNo As String = RetriveJobNumber(oDrawDoc)

        Dim JobSheetComponents As List(Of Component) = RetrievePartsList(jobNo)

        Dim ThisApplication As Application = oDrawDoc.Parent

        ' Set a reference to the active sheet.
        Dim oSheet As Sheet
        oSheet = oDrawDoc.ActiveSheet

        If oSheet.CustomTables.Count > 0 Then
            oSheet.CustomTables.Item(1).Delete()
        End If

        'Set the column titles & Create the custom table
        Dim oInspTblTitle As String
        oInspTblTitle = "PARTS LIST"

        Dim oTitles(3) As String
        oTitles(0) = "ITEM"
        oTitles(1) = "QTY"
        oTitles(2) = "DESCRIPTION"
        oTitles(3) = "PART NUMBER"

        Dim ColumnWidths(3) As Double
        ColumnWidths(0) = 2
        ColumnWidths(1) = 2
        ColumnWidths(2) = 8
        ColumnWidths(3) = 10

        Dim ContentsSize As Integer = (JobSheetComponents.Count * 4 - 1)

        Dim Contents(ContentsSize) As String

        'Dim i As Integer = 0

        'For Each comp In JobSheetComponents

        '    Contents(i) = comp.ItemNumber
        '    Contents(i + 1) = comp.Qty.ToString
        '    Contents(i + 2) = comp.Description
        '    Contents(i + 3) = comp.PartNumber

        '    i = i + 4

        'Next

        'Reverse direction of PartsList

        Dim i As Integer = ContentsSize

        For Each comp In JobSheetComponents

            Contents(i - 3) = comp.ItemNumber
            Contents(i - 2) = comp.Qty.ToString
            Contents(i - 1) = comp.Description
            Contents(i) = comp.PartNumber

            i = i - 4

        Next

        'oDrawDoc.StylesManager.ActiveStandardStyle.ActiveObjectDefaults.TableStyle.ColumnValueHorizontalJustification = HorizontalTextAlignmentEnum.kAlignTextLeft

        Dim oInspDimTable As CustomTable

        'Change this logic to place in to right bottom corner

        ' Set a reference to the sheet's border
        Dim oBorder As Border
        oBorder = oSheet.Border

        Dim oTitleBlock As TitleBlock
        oTitleBlock = oSheet.TitleBlock

        Dim xrev As Double = oBorder.RangeBox.MaxPoint.X
        Dim yrev As Double = oTitleBlock.RangeBox.MaxPoint.Y

        Dim pnt As Point2d = ThisApplication.TransientGeometry.CreatePoint2d(xrev, yrev)

        oInspDimTable = oSheet.CustomTables.Add(oInspTblTitle, pnt, 4, JobSheetComponents.Count, oTitles, Contents, ColumnWidths)

        oInspDimTable.Columns.Item(3).ValueHorizontalJustification = HorizontalTextAlignmentEnum.kAlignTextLeft
        oInspDimTable.Columns.Item(4).ValueHorizontalJustification = HorizontalTextAlignmentEnum.kAlignTextLeft

        'May be possible to speed this up by adding into the MoreInfo parameter of the CustomTables.Add Method
        oInspDimTable.HeadingPlacement = HeadingPlacementEnum.kHeadingAtBottom

        Dim tableRangeBox As Box2d = oInspDimTable.RangeBox

        Dim newPnt As Point2d = ThisApplication.TransientGeometry.CreatePoint2d(xrev - (tableRangeBox.MaxPoint.X - tableRangeBox.MinPoint.X), yrev + (tableRangeBox.MaxPoint.Y - tableRangeBox.MinPoint.Y))

        oInspDimTable.Position = newPnt

    End Sub

    Public Sub OutputTubeSizeList(invDoc As AssemblyDocument)

        ' set a reference to the assembly component definintion.
        ' This assumes an assembly document is open.
        Dim oAsmCompDef As AssemblyComponentDefinition
        oAsmCompDef = invDoc.ComponentDefinition

        'Make sure list of pipe components is empty
        pipeComponents.Clear()

        'Iterate through all of the occurrences
        Dim oOccurrence As ComponentOccurrence
        For Each oOccurrence In oAsmCompDef.Occurrences
            If oOccurrence.Suppressed = False And oOccurrence.BOMStructure <> BOMStructureEnum.kReferenceBOMStructure Then
                If Not TypeOf oOccurrence.Definition Is VirtualComponentDefinition Then
                    processAllSubOcc(oOccurrence)
                End If
            End If
        Next

        'For Each component In pipeComponents
        '    Console.WriteLine(component.Qty & "..." & component.Description & "..." & component.PartNumber)
        'Next

        'Console.Write("Press Enter to continue...")
        'Console.ReadLine()

        'Dim xlWorkSheet As Excel.Worksheet = ConnectToJobSheet(RetriveJobNumber(invDoc))
        Dim xlWorkSheet As Excel.Worksheet = ConnectToActiveWorksheet()

        If xlWorkSheet IsNot Nothing Then
            OutputToJobSheet("Pipework and Hose", pipeComponents, xlWorkSheet)
        End If

        'xlWorkBook.Close(False) ' False = Do not save
        'xlApp.Quit()

        releaseObject(xlWorkSheet)

    End Sub

    'This might be frowned upon as it only works when the JobSheet AddIn is disabled
    Public Sub OpenJobSheet(invDoc As Document)

        Dim xlWorkSheet As Excel.Worksheet = ConnectToJobSheet(RetriveJobNumber(invDoc))

        releaseObject(xlWorkSheet)

    End Sub

    Private Function ConnectToJobSheet(jobSheet As String)

        Dim xlApp As Excel.Application

        Try
            Dim oForm As ConnectToExcelWarningDialog = New ConnectToExcelWarningDialog()
            oForm.Show()
            xlApp = Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application")
            oForm.Close()
            oForm = Nothing

        Catch

            Dim xlAppType As Type =
                  GetTypeFromProgID("Excel.Application")

            xlApp = CreateInstance(xlAppType)

        End Try

        xlApp.Visible = True

        'Change security settings of Excel session to disable Jobsheet add-on
        xlApp.AutomationSecurity = Microsoft.Office.Core.MsoAutomationSecurity.msoAutomationSecurityForceDisable '???

        Dim JobSheetAndPath As String = "\\denleyserver\jobsheets\" & jobSheet & ".xls"

        Dim xlWorkBook As Excel.Workbook

        Dim workSheetAlreadyOpen As Boolean = False

        For Each workbook In xlApp.Workbooks
            If workbook.Name = jobSheet & ".xls" Then
                workSheetAlreadyOpen = True
                xlWorkBook = workbook
                Exit For
            End If
        Next

        If Not (workSheetAlreadyOpen) Then
            xlWorkBook = xlApp.Workbooks.Open(JobSheetAndPath, ,) 'Leaving 3rd Input Param blank makes it Writable
        End If

        Dim xlWorkSheet As Excel.Worksheet

        xlWorkSheet = xlWorkBook.Worksheets("JobSheet")

        Return xlWorkSheet

    End Function

    Private Function ConnectToActiveWorksheet()

        Dim xlApp As Excel.Application
        Dim retryExcel As Boolean = True

        While (retryExcel)

            Try
                Dim oForm As ConnectToExcelWarningDialog = New ConnectToExcelWarningDialog()
                'oForm.Show()
                xlApp = Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application")
                'oForm.Close()
                'oForm = Nothing

                xlApp.Visible = True

                retryExcel = False
            Catch
                Select Case MsgBox("Failed to connect to a session of Excel.  Retry?", MsgBoxStyle.YesNoCancel, "Denley Tools")
                    Case vbNo, vbCancel
                        retryExcel = False
                End Select
            End Try

        End While

        Dim xlWorkSheet As Excel.Worksheet

        If xlApp IsNot Nothing Then

            Dim retryWorkbook As Boolean = True
            While (retryWorkbook)
                Try
                    xlWorkSheet = xlApp.ActiveWorkbook.ActiveSheet
                    retryWorkbook = False
                Catch
                    Select Case MsgBox("Failed to connect to the active workbook.  Retry?", MsgBoxStyle.YesNoCancel, "Denley Tools")
                        Case vbNo, vbCancel
                            retryWorkbook = False
                    End Select
                End Try
            End While

        End If

        Return xlWorkSheet

    End Function

    Private Sub OutputToJobSheet(listName As String, componentList As List(Of Component), xlWorkSheet As Excel.Worksheet)

        Dim finalRow As Integer = 0
        Dim j As Integer

        'Check first 4 columns for final row used
        For j = 1 To 4
            Dim thisColumnFinalRow As Integer = xlWorkSheet.Cells(xlWorkSheet.Rows.Count, j).End(Excel.XlDirection.xlUp).Row

            If thisColumnFinalRow > finalRow Then
                finalRow = thisColumnFinalRow
            End If
        Next


        Dim finalPartNo As Integer = 0

        Try
            finalPartNo = CInt(xlWorkSheet.Range("A" & finalRow).Value)
        Catch
        End Try

        xlWorkSheet.Range("C" & finalRow + 2).Value = listName
        xlWorkSheet.Range("C" & finalRow + 2).Font.Bold = True

        Dim i As Integer = 1
        For Each comp In componentList

            Dim thisRow As Integer = finalRow + 2 + i

            xlWorkSheet.Range("A" & thisRow).Value = CInt(finalPartNo + i)

            xlWorkSheet.Range("B" & thisRow).Value = comp.Qty
            xlWorkSheet.Range("C" & thisRow).Value = comp.Description
            xlWorkSheet.Range("D" & thisRow).Value = comp.PartNumber

            'Cert Level
            xlWorkSheet.Range("K" & thisRow).Value = 1

            i = i + 1
        Next

    End Sub

    'The way that this method is written will mean that the WriteTubeProps sub will only run on bottom level components (i.e. parts or empty assemblies)
    Private Sub processAllSubOcc(oCompOcc As ComponentOccurrence)

        WriteTubeProps(oCompOcc.Definition.Document)

        Dim oSubCompOcc As ComponentOccurrence

        For Each oSubCompOcc In oCompOcc.SubOccurrences

            If oSubCompOcc.Suppressed = False And oSubCompOcc.BOMStructure <> BOMStructureEnum.kReferenceBOMStructure Then
                If oSubCompOcc.SubOccurrences.Count = 0 Then
                    If Not TypeOf oSubCompOcc.Definition Is VirtualComponentDefinition Then
                        Dim occDoc As Document = oSubCompOcc.Definition.Document
                        WriteTubeProps(occDoc)
                    End If
                ElseIf oSubCompOcc.BOMStructure <> BOMStructureEnum.kInseparableBOMStructure Then
                    Call processAllSubOcc(oSubCompOcc)
                End If
            End If

        Next

    End Sub

    Private Sub WriteTubeProps(occDoc As Document)

        Dim fullDocumentName As String = occDoc.FullDocumentName

        If fullDocumentName.Contains("Boss") = False Then

            If fullDocumentName.Contains("pisweep") Or fullDocumentName.Contains("Metric Tube") Or fullDocumentName.Contains("Downpipe") Then

                Dim invParams As Parameters = occDoc.ComponentDefinition.Parameters
                Dim pipeSizeAlreadyInList As Boolean = False
                Dim pipeSize As String
                Dim pipeLength As Double
                Dim pipeLengthInMili As Double

                If fullDocumentName.Contains("pisweep") Then

                    Dim invProps As PropertySet = occDoc.PropertySets.Item("User Defined Properties")

                    pipeSize = invParams.Item("OD").Value * 10 & "mm O/D x " & invParams.Item("t").Value * 10 & "mm Wall"

                    pipeLengthInMili = CDbl(Replace(invProps.Item("PL").Value, " mm", ""))

                Else

                    pipeSize = invParams.Item("OD").Value * 10 & "mm O/D x " & invParams.Item("Wall").Value * 10 & "mm Wall"

                    If fullDocumentName.Contains("x90") Then
                        Try
                            pipeLengthInMili = RetrieveAngledPipeLength(invParams)
                        Catch
                            pipeLengthInMili = invParams.Item("Length").Value * 10 'cm to mm
                        End Try
                    Else
                        Try
                            pipeLengthInMili = invParams.Item("Length").Value * 10 'cm to mm
                        Catch
                            pipeLengthInMili = RetrieveAngledPipeLength(invParams)
                        End Try
                    End If

                End If

                Dim pipeLengthInMeterTenths = pipeLengthInMili / 100

                pipeLength = Math.Ceiling(pipeLengthInMeterTenths) / 10

                Dim i As Integer = 0
                For Each pipe In pipeComponents
                    If pipeSize.Equals(pipe.PartNumber) Then
                        pipeSizeAlreadyInList = True
                        pipeLength = CDbl(Replace(pipe.Qty, "m", "")) + CDbl(Replace(pipeLength, "m", ""))
                        Exit For
                    End If
                    i = i + 1
                Next

                Dim pipeComponent As New Component With {
                        .Qty = pipeLength & "m",
                        .Description = "Steel Tube",
                        .PartNumber = pipeSize
                    }

                If pipeSizeAlreadyInList Then
                    pipeComponents(i) = pipeComponent
                Else
                    pipeComponents.Add(pipeComponent)
                End If

            ElseIf fullDocumentName.Contains("pifhose") Then

                'For every pifhose of a particular size, we want to add a 'Hydraulic Hose Ass'y' to JobSheet

                Dim hoseSizeAlreadyInList As Boolean = False
                Dim stockNo As String = occDoc.PropertySets.Item("Design Tracking Properties").Item("Stock Number").Value

                Dim hoseQty As Integer = 1

                Dim i As Integer = 0
                For Each hose In pipeComponents
                    If stockNo.Equals(hose.PartNumber) Then
                        hoseSizeAlreadyInList = True
                        hoseQty = CDbl(hose.Qty) + 1
                        Exit For
                    End If
                    i = i + 1
                Next

                Dim hoseComponent As New Component With {
                    .Qty = hoseQty,
                    .Description = "Hydraulic Hose Ass'y",
                    .PartNumber = stockNo
                }

                If hoseSizeAlreadyInList Then
                    pipeComponents(i) = hoseComponent
                Else
                    pipeComponents.Add(hoseComponent)
                End If

            End If

        End If

    End Sub

    Private Function RetrieveAngledPipeLength(invParams As Parameters)

        Dim pipeLengthInMili As Double

        Dim Leg1_Z As Double = invParams.Item("Leg1_Z").Value * 10 'cm to mm
        Dim Leg2_Y As Double = invParams.Item("Leg2_Y").Value * 10 'cm to mm
        Dim radius As Double = invParams.Item("Rad").Value * 10 'cm to mm

        pipeLengthInMili = Leg1_Z + Leg2_Y - radius * 2 + (Math.PI * radius / 2)

        Return pipeLengthInMili

    End Function

    Private Function RetriveJobNumber(invDoc As Document)

        Dim filepathArr As String() = invDoc.FullFileName.Split("\")

        Dim folderName As String = filepathArr(filepathArr.Length - 2)

        Dim jobNo As String = Left(folderName, 5)

        If Char.IsLetter(folderName.Chars(5)) Then
            jobNo = jobNo & folderName.Chars(5)
        End If

        Return jobNo

    End Function

    Public Sub OutputFittingList(invDoc As AssemblyDocument)



        'Make sure list of pipe components is empty
        fittingComponents.Clear()

        Dim structuredFittingList As Boolean

        Dim result As MsgBoxResult = MsgBox("Do you want the Fitting list to be split by sub-assemblies and folders?", MsgBoxStyle.YesNoCancel, "Output Fitting List")
        If result = MsgBoxResult.Yes Then
            structuredFittingList = True
        ElseIf MsgBoxResult.No Then
            structuredFittingList = False
        Else
            Exit Sub
        End If

        'Drop the additional fittings into their own folder at the end?
        RetrieveFittingList(invDoc, True, structuredFittingList)

        'Dim xlWorkSheet As Excel.Worksheet = ConnectToJobSheet(RetriveJobNumber(invDoc))
        Dim xlWorkSheet As Excel.Worksheet = ConnectToActiveWorksheet()

        If xlWorkSheet IsNot Nothing Then

            Dim subList As JobSheet.ComponentSubList

            For Each subList In fittingComponents

                'Sort by description the Part Number
                subList.components = subList.components.OrderBy(Function(x) x.PartNumber).ToList()
                subList.components = subList.components.OrderBy(Function(x) x.Description).ToList()

                'Write each of the lists to the JobSheet
                OutputToJobSheet(subList.listName, subList.components, xlWorkSheet)

            Next

        End If

        releaseObject(xlWorkSheet)

    End Sub

    Private Sub RetrieveFittingList(invDoc As AssemblyDocument, isTopLevel As Boolean, structuredFittingList As Boolean)

        Dim oDef As AssemblyComponentDefinition = invDoc.ComponentDefinition

        Dim oPane As BrowserPane = invDoc.BrowserPanes.ActivePane

        Dim oOccurrenceNodes As ObjectCollection = invDoc.Parent.TransientObjects.CreateObjectCollection

        Dim filenameArr = invDoc.FullFileName.Split("\")
        Dim fileName As String = filenameArr(filenameArr.Length - 1)
        fileName = Left(fileName, fileName.Length - 4)

        Dim oOcc As ComponentOccurrence

        For Each oOcc In oDef.Occurrences

            If oOcc.Suppressed = False And oOcc.BOMStructure <> BOMStructureEnum.kReferenceBOMStructure And oOcc.Name <> "" Then

                Dim oNode As BrowserNode = oPane.GetBrowserNodeFromObject(oOcc)
                oOccurrenceNodes.Add(oNode)

                Dim parentNodeString As String = oNode.Parent.FullPath

                'This IF statement works out if the parent is a folder
                If parentNodeString.Contains(fileName) And parentNodeString.Contains(":") And Not (parentNodeString.Contains("Pattern")) Then 'Must be a folder

                    Dim filenameArrB = parentNodeString.Split(":")
                    Dim folderName As String

                    folderName = filenameArrB(1)

                    AddToFittingComponents(oOcc, folderName, structuredFittingList)

                ElseIf oOcc.DefinitionDocumentType = DocumentTypeEnum.kAssemblyDocumentObject And oOcc.BOMStructure <> BOMStructureEnum.kInseparableBOMStructure Then 'Sub Assembly


                    Call RetrieveFittingList(oOcc.Definition.Document, False, structuredFittingList)

                Else

                    If isTopLevel Then
                        AddToFittingComponents(oOcc, "Fittings", structuredFittingList)
                    Else
                        AddToFittingComponents(oOcc, invDoc.DisplayName, structuredFittingList)
                    End If

                End If

            End If

        Next

    End Sub


    Private Sub AddToFittingComponents(oOcc As ComponentOccurrence, listName As String, structuredFittingList As Boolean)

        Dim invDoc As Document = oOcc.Definition.Document

        Dim partNo As String = invDoc.PropertySets.Item("Design Tracking Properties").Item("Part Number").Value
        Dim partDesc As String = invDoc.PropertySets.Item("Design Tracking Properties").Item("Description").Value
        Dim costCenter As String = invDoc.PropertySets.Item("Design Tracking Properties").Item("Cost Center").Value

        If structuredFittingList = False Then listName = "Fittings"

        If Left(partNo, 15) = "Stud Barrel Tee" Then
            partNo = Right(partNo, partNo.Length - 16)
            partDesc = "Stud Barrel Tee"
        End If

        If (costCenter = "Fitting" And partDesc.Contains("Hose") = False) Or IsFitting(partDesc) Then

            Dim listNameExisting As Boolean = False

            Dim thisList As ComponentSubList

            For Each list In fittingComponents
                If list.listName = listName Then
                    listNameExisting = True
                    thisList = list
                    Exit For
                End If
            Next

            If listNameExisting = False Then
                thisList = New ComponentSubList(listName)
                fittingComponents.Add(thisList)
            End If

            Dim pipeSizeAlreadyInList As Boolean = False

            Dim i As Integer = 0
            For Each component In thisList.components
                If partNo.Equals(component.PartNumber) And partDesc.Equals(component.Description) Then
                    pipeSizeAlreadyInList = True

                    Dim fittingComponent As New Component With {
                        .Qty = component.Qty + 1,
                        .Description = partDesc,
                        .PartNumber = partNo
                    }

                    thisList.components(i) = fittingComponent

                    Exit For
                End If
                i = i + 1
            Next

            If pipeSizeAlreadyInList = False Then

                Dim fittingComponent As New Component With {
                        .Qty = 1,
                        .Description = partDesc,
                        .PartNumber = partNo
                    }

                thisList.components.Add(fittingComponent)

            End If

        End If

    End Sub

    Private Function IsFitting(desc As String)

        Dim thisIsFitting As Boolean = False

        If desc.Contains("Flange") Or desc.Contains("Elbow") Or desc.Contains("Adaptor") Or desc.Contains("Coupling") Or desc.Contains("Bonded Washer") Or desc.Contains("Equal Tee") Or desc.Contains("Swivel Female") Or desc.Contains("Pipe Clamp") Or desc.Contains("Standpipe") Or desc.Contains("Test Point") Or desc.Contains("Tube Reducer") Or desc.Contains("Stud Barrel Tee") Or desc.Contains("Bulkhead Union") Then 'Or desc.Contains("Boss") 
            thisIsFitting = True
        End If

        Return thisIsFitting

    End Function

    Private Function IsBoss(desc As String)

        Dim thisIsBoss As Boolean = False

        If desc.Contains("Boss") Then
            thisIsBoss = True
        End If

        Return thisIsBoss

    End Function

End Module
