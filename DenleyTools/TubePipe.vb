﻿Imports Inventor

Public Module TubePipe

    'Don't bother with place tube buttons as it seems difficult.  Placing from Content Centre works fine and it fairly quick.
    'Just make it so that the content centre tubes will be added to pipe list.

    Public Sub PlaceMetrixTube(invDoc As AssemblyDocument)


        'invDoc.ComponentDefinition.Occurrences.Add("G:\Inventor\Templates\R2017 Templates\Metric Tube\Metric Tube.ipt")

        'invDoc.ComponentDefinition.Occurrences.AddCustomiPartMember()

        'Try place from Content Centre
        'https://forums.autodesk.com/t5/inventor-customization/ilogic-to-trigger-and-insert-custom-content-center-files-to-an/td-p/3916122

        'PlacePartInteractive(invDoc.Parent, "G:\Inventor\Templates\R2017 Templates\Metric Tube\Metric Tube.ipt")

    End Sub

    Public Sub PlaceMetrixTubex90(invDoc As AssemblyDocument)



    End Sub

    Private Sub PlacePartInteractive(invApp As Application, fileName As String)

        ' Post the filename to the private event queue. 
        Dim cmdMgr As CommandManager
        cmdMgr = invApp.CommandManager
        cmdMgr.PostPrivateEvent(PrivateEventTypeEnum.kFileNameEvent, fileName)

        ' Execute the "Place Component" command. 
        cmdMgr.ControlDefinitions.Item("AssemblyPlaceComponentCmd").Execute()
    End Sub

    Public Sub TubeHoseVelocityCheck()

        'allow drop down to use hose/pipes from 'Tube & Pipe' library.
        'would be much better as a form which updates when values change

        Dim title As String = "Hydraulic Tube & Hose Velocity Check"

        Dim flowRate As Double = InputBox("Flow Rate (litres/min)", title, "4")

        Dim bore As Double = InputBox("Bore (mm)", title, "9")

        Dim flowVelocity As Double = (4 * flowRate / 60) / (Math.PI * bore ^ 2) * 1000 'mm to m

        MsgBox("Flow Velocity:" & flowVelocity & "(m/s)" & vbNewLine & vbNewLine _
        & "Recommended for Suction: 0.6 to 1.2m/s" & vbNewLine _
        & "Recommended for Pressure: 2.0 to 4.5m/s")

    End Sub

    Public Sub GroundTubePipe(invDoc As AssemblyDocument)

        Dim compOcc As ComponentOccurrence

        For Each compOcc In invDoc.ComponentDefinition.Occurrences

            If Left(compOcc.Name, 16) = "Tube & Pipe Runs" Then

                Dim topLevelCompOccDef As AssemblyComponentDefinition = invDoc.ComponentDefinition
                Dim topLevelYZPlane As WorkPlane = topLevelCompOccDef.WorkPlanes(1)
                Dim topLevelXZPlane As WorkPlane = topLevelCompOccDef.WorkPlanes(2)
                Dim topLevelXYPlane As WorkPlane = topLevelCompOccDef.WorkPlanes(3)

                Dim tubePipeCompOccDef As AssemblyComponentDefinition = compOcc.Definition
                Dim compOccYZPlane As WorkPlane = tubePipeCompOccDef.WorkPlanes(1)
                Dim compOccXZPlane As WorkPlane = tubePipeCompOccDef.WorkPlanes(2)
                Dim compOccXYPlane As WorkPlane = tubePipeCompOccDef.WorkPlanes(3)

                Dim compOccYZPlaneProxy
                compOcc.CreateGeometryProxy(compOccYZPlane, compOccYZPlaneProxy)

                Dim compOccXZPlaneProxy
                compOcc.CreateGeometryProxy(compOccXZPlane, compOccXZPlaneProxy)

                Dim compOccXYPlaneProxy
                compOcc.CreateGeometryProxy(compOccXYPlane, compOccXYPlaneProxy)

                Dim invProps As PropertySet = tubePipeCompOccDef.Document.PropertySets.Item("User Defined Properties")

                Dim grounded As Boolean = False

                Try
                    grounded = invProps.Item("Grounded").Value
                Catch
                End Try

                If grounded = False Then
                    topLevelCompOccDef.Constraints.AddFlushConstraint(topLevelYZPlane, compOccYZPlaneProxy, 0)
                    topLevelCompOccDef.Constraints.AddFlushConstraint(topLevelXZPlane, compOccXZPlaneProxy, 0)
                    topLevelCompOccDef.Constraints.AddFlushConstraint(topLevelXYPlane, compOccXYPlaneProxy, 0)

                    Try
                        grounded = invProps.Item("Grounded").Value = True
                    Catch
                        invProps.Add(True, "Grounded")
                    End Try

                End If

                Dim subCompOcc As ComponentOccurrence

                For Each subCompOcc In tubePipeCompOccDef.Occurrences

                    'Ground all assemblies (the Runs) within the Tube & Pipe Assembly
                    If subCompOcc.DefinitionDocumentType = DocumentTypeEnum.kAssemblyDocumentObject And subCompOcc.Suppressed = False Then
                        subCompOcc.Grounded = True
                    End If

                Next

            ElseIf compOcc.DefinitionDocumentType = DocumentTypeEnum.kAssemblyDocumentObject And compOcc.Suppressed = False Then

                Dim assDoc As AssemblyDocument = compOcc.Definition.Document

                If assDoc.SubType <> "{28EC8354-9024-440F-A8A2-0E0E55D635B0}" Then ' Weldment
                    Call GroundTubePipe(compOcc.Definition.Document)
                End If

            End If

        Next

    End Sub

End Module
