﻿Imports Inventor

Public Module TableDriven

    Dim invApp As Application

    Public Sub TableBreaker(invDoc As Document)

        invApp = invDoc.Parent

        invApp.UserInterfaceManager.UserInteractionDisabled = True
        invApp.SilentOperation = True

        Select Case invDoc.DocumentType
            Case DocumentTypeEnum.kPartDocumentObject
                Dim partDoc As PartDocument = invDoc
                If partDoc.ComponentDefinition.IsiPartFactory Then
                    PartTableBreaker(partDoc)
                Else
                    MsgBox("Isn't an iPart Factory")
                End If
            Case DocumentTypeEnum.kAssemblyDocumentObject
                Dim assDoc As AssemblyDocument = invDoc
                If assDoc.ComponentDefinition.IsiAssemblyFactory Then
                    AssemblyTableBreaker(assDoc)
                Else
                    MsgBox("Isn't an iAssembly Factory")
                End If
            Case Else
                MsgBox("Not a part or assembly")
        End Select

        invApp.UserInterfaceManager.UserInteractionDisabled = False
        invApp.SilentOperation = False

    End Sub

    Private Sub PartTableBreaker(invDoc As PartDocument)

        Dim oPartFactory As iPartFactory = invDoc.ComponentDefinition.iPartFactory

        Dim factoryFileName As String = invDoc.FullFileName

        Dim oRow As iPartTableRow

        For Each oRow In oPartFactory.TableRows

            oPartFactory.DefaultRow = oRow

            Dim desiredFileName As String = Left(factoryFileName, factoryFileName.Length - 4) & "\" & oRow.PartName

            If Right(desiredFileName, 4) <> ".ipt" Then
                desiredFileName = desiredFileName & ".ipt"
            End If

            Dim fileSaved As Boolean = False

            While Not (fileSaved)

                Try
                    invDoc.SaveAs(desiredFileName, True)
                    fileSaved = True
                Catch
                    Select Case MsgBox(desiredFileName & " failed to save. May be locked by Vault. Retry?", MsgBoxStyle.YesNoCancel, "Table Breaker")
                        Case MsgBoxResult.Yes
                            'Do Nothing
                        Case MsgBoxResult.No
                            Continue For
                        Case MsgBoxResult.Cancel
                            Exit Sub
                    End Select
                End Try

            End While

            Dim memberDoc As PartDocument = invApp.Documents.Open(desiredFileName, False)

            'Delete table to convert to a standard part file
            If memberDoc.ComponentDefinition.IsiPartFactory Then
                memberDoc.ComponentDefinition.iPartFactory.Delete()
            End If

            DeleteSuppressedFeatures(memberDoc)

            memberDoc.Save()

            memberDoc.Close()

        Next

    End Sub

    Private Sub AssemblyTableBreaker(invDoc As AssemblyDocument)

        Dim oAssFactory As iAssemblyFactory = invDoc.ComponentDefinition.iAssemblyFactory

        Dim factoryFileName As String = invDoc.FullFileName

        Dim oRow As iAssemblyTableRow

        For Each oRow In oAssFactory.TableRows

            oAssFactory.DefaultRow = oRow

            Dim desiredFileName As String = Left(factoryFileName, factoryFileName.Length - 4) & "\" & oRow.DocumentName & ".iam"

            Dim fileSaved As Boolean = False

            'Dim temp As Integer = 0

            While Not (fileSaved) 'And temp < 10

                Try
                    oAssFactory.CreateMember(oRow)
                    fileSaved = True
                Catch
                    Select Case MsgBox(desiredFileName & " failed to save. May be locked by Vault. Retry?", MsgBoxStyle.YesNoCancel, "Table Breaker")
                        Case MsgBoxResult.Yes
                            'Do Nothing
                        Case MsgBoxResult.No
                            Continue For
                        Case MsgBoxResult.Cancel
                            Exit Sub
                    End Select
                End Try

                'temp = temp + 1

            End While

            Dim memberDoc As AssemblyDocument = invApp.Documents.Open(desiredFileName, False)

            If memberDoc.ComponentDefinition.IsiAssemblyMember Then
                memberDoc.ComponentDefinition.iAssemblyMember.BreakLinkToFactory()

                Try
                    'add a new paramter to ensure document change so that a save happens
                    memberDoc.ComponentDefinition.Parameters.UserParameters.AddByValue("update", 0, "ul")
                Catch
                End Try

            End If

            memberDoc.Save()

            memberDoc.Close()

        Next

    End Sub

    Private Sub DeleteSuppressedFeatures(invDoc As PartDocument)

        Dim oFeature As PartFeature
        Dim oFeatures As PartFeatures

        oFeatures = invDoc.ComponentDefinition.Features

        For Each oFeature In oFeatures
            Try
                If oFeature.Suppressed = True Then oFeature.Delete()
            Catch
            End Try
        Next

        Dim oSketch As PlanarSketch
        Dim oSketches As PlanarSketches
        oSketches = invDoc.ComponentDefinition.Sketches

        For Each oSketch In oSketches
            Try
                If oSketch.Dependents.Count = 0 Then
                    oSketch.Delete()
                Else
                    oSketch.Visible = False
                End If

            Catch
            End Try
        Next

    End Sub

End Module
