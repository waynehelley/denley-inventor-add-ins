﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventor;
using DenleyTools_CSharp;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace InventorTestModule_CSharp
{
    class TestModule
    {

        static Inventor.Application _invApp;        

        static void Main(string[] args)
        {

            ConnectInventor();

            Document invDoc = _invApp.ActiveDocument;            

            CushionedCylinder cylinder = new DenleyTools_CSharp.CushionedCylinder((AssemblyDocument)invDoc);                           

        }

        static bool ConnectInventor()
        {
            try
            {
                try
                {
                    // Get active inventor object
                    _invApp = System.Runtime.InteropServices.Marshal.GetActiveObject("Inventor.Application") as Inventor.Application;
                }
                catch (COMException)
                {
                    MessageBox.Show("AutoBolts : Inventor must be running.");
                    return false;
                }
                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }

            return true;
        }

    }
}
