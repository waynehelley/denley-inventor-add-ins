﻿Imports System
Imports System.Type
Imports System.Activator
Imports System.Runtime.InteropServices
Imports AutoCAD

Module AutoCADTestModule

    Dim _acApp As AcadApplication
    Dim _started As Boolean

    Sub Main()

        ' Add any initialization after the InitializeComponent() call.
        Try
            _acApp = Marshal.GetActiveObject("AutoCAD.Application")

        Catch ex As Exception
            Try
                Dim invAppType As Type =
                  GetTypeFromProgID("AutoCAD.Application")

                _acApp = CreateInstance(invAppType)
                _acApp.Visible = True

                'Note: if you shut down the Inventor session that was started
                'this(way) there is still an Inventor.exe running. We will use
                'this Boolean to test whether or not the Inventor App  will
                'need to be shut down.
                _started = True

            Catch ex2 As Exception
                MsgBox(ex2.ToString())
                MsgBox("Unable to get or start Inventor")
            End Try
        End Try

        Dim acDoc As AcadDocument = _acApp.ActiveDocument

        MsgBox(acDoc.FullName)

        'Release references
        _acApp = Nothing

    End Sub

End Module